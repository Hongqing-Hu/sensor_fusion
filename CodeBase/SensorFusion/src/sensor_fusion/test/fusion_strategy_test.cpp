#include "sensor_fusion/SlowFusionStrategy.h"
#include <gtest/gtest.h>
#include <ros/ros.h>

using namespace sensor_fusion;

TEST(FusionStrategyTest, SlowFusionStrategy_Syntetic)
{
    SlowFusionStrategy algo(0.5);
    std::list<SensorData> fusion_in;
    SensorData fusion_out;
    // throw on empty input
    ASSERT_ANY_THROW(algo.algorithm(fusion_in, fusion_out));

    SensorData empty;
    empty.timestamp = 1;
    fusion_in.push_back(empty);
    // throw on input of size 1
    ASSERT_ANY_THROW(algo.algorithm(fusion_in, fusion_out));

    fusion_in.push_back(empty);
    algo.algorithm(fusion_in, fusion_out);
    ASSERT_TRUE(fusion_out.sensor_list_data.size() == 0);

    SensorPoint sp1, sp2;
    sp1.object_id = 1;
    sp1.x_position = 2;
    sp1.y_position = 3;

    sp2.object_id = 2;
    sp2.x_position = 3;
    sp2.y_position = 4;

    fusion_in.front().sensor_list_data.push_back(sp1);
    fusion_in.back().sensor_list_data.push_back(sp2);
    algo.algorithm(fusion_in, fusion_out);
    // two sufficiently distant objects do not fuse with each other
    ASSERT_TRUE(fusion_out.sensor_list_data.size() == 0);

    fusion_in.push_back(empty);
    fusion_in.back().sensor_list_data.push_back(sp1);
    algo.algorithm(fusion_in, fusion_out);
    // now one object in common
    ASSERT_TRUE(fusion_out.sensor_list_data.size() == 1);
    EXPECT_DOUBLE_EQ(fusion_out.sensor_list_data[0].x_position, sp1.x_position);
    EXPECT_DOUBLE_EQ(fusion_out.sensor_list_data[0].y_position, sp1.y_position);

//    fusion_in.front().sensor_list_data.push_back(sp2);
//    algo.algorithm(fusion_in, fusion_out);
//    // now two objects in common
//    ASSERT_TRUE(fusion_out.sensor_list_data.size() == 2);
//    EXPECT_DOUBLE_EQ(fusion_out.sensor_list_data[0].x_position, sp1.x_position);
//    EXPECT_DOUBLE_EQ(fusion_out.sensor_list_data[0].y_position, sp1.y_position);
//    EXPECT_DOUBLE_EQ(fusion_out.sensor_list_data[1].x_position, sp2.x_position);
//    EXPECT_DOUBLE_EQ(fusion_out.sensor_list_data[1].y_position, sp2.y_position);


}




// Run all the tests that were declared with TEST()
int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
