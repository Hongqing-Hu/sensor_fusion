#include "sensor_fusion/SensorFusion.h"
#include <gtest/gtest.h>
#include <ros/ros.h>

using namespace sensor_fusion;

TEST(SensorFusionTest, SensorFusion_Basic)
{
  SensorFusion fusion(0.5);

  // given input
  std::unordered_map<SensorType, SensorData> input;
  SensorData input_data;

  EXPECT_ANY_THROW(SensorData output = fusion.fuse(input));


  input_data.timestamp = -10;
  input[SensorType::SHORT_RANGE_RADAR] = input_data;
  input[SensorType::LONG_RANGE_RADAR] = input_data;
  input_data.timestamp = 10;
  input[SensorType::CAMERA] = input_data;
  SensorData output = fusion.fuse(input);

  EXPECT_EQ(output.sensor_list_data.size(), 0);
  EXPECT_EQ(output.timestamp, 10);

  // let's add a point
  SensorPoint input_point;
  input_point.x_position = 0;
  input_point.y_position = 0;

  input_data.sensor_list_data.push_back(input_point);
  input[SensorType::CAMERA] = input_data;
  output = fusion.fuse(input);
  // no respective radar objects
  EXPECT_EQ(output.sensor_list_data.size(), 0);


  input[SensorType::SHORT_RANGE_RADAR] = input_data;
  output = fusion.fuse(input);
  // now we have one corresponiding pair
  EXPECT_EQ(output.sensor_list_data.size(), 1);

  input_point.x_position = 10;
  input_point.y_position = 10;
  input_data.sensor_list_data.push_back(input_point);
  input[SensorType::CAMERA] = input_data;
  output = fusion.fuse(input);
  // still one pair
  EXPECT_EQ(output.sensor_list_data.size(), 1);

  input_data.sensor_list_data[1].x_position = 10.499;
  input[SensorType::LONG_RANGE_RADAR] = input_data;
  output = fusion.fuse(input);
  // now should have 2 pairs
  EXPECT_EQ(output.sensor_list_data.size(), 2);
}


// Run all the tests that were declared with TEST()
int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
