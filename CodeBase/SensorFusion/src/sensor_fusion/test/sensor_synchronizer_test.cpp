#include "sensor_fusion/SensorSynchronizer.h"
#include <gtest/gtest.h>
#include <ros/ros.h>

using namespace sensor_fusion;

/// Tests provided instance of SensorSynchronizer class
void test_synchro(SensorSynchronizer &synchro)
{
  std::map<timestamp_t, std::unordered_map<SensorType, SensorData>> output;
  output = synchro.synchronize();
  // empty input -> empty output
  EXPECT_EQ(output.size(), 0);

  SensorData data;
  data.timestamp = 0;
  synchro.addSensorData(SensorType::CAMERA, data);
  output = synchro.synchronize();
  // only camera input -> empty output
  EXPECT_EQ(output.size(), 0);

  data.timestamp = 0.04;
  synchro.addSensorData(SensorType::LONG_RANGE_RADAR, data);
  output = synchro.synchronize();
  // camera + long range as input -> still not enough for synchro
  EXPECT_EQ(output.size(), 0);

  data.timestamp = 0.06;
  synchro.addSensorData(SensorType::SHORT_RANGE_RADAR, data);
  output = synchro.synchronize();
  // short range is not within the valid range -> no synchro yet
  EXPECT_EQ(output.size(), 0);

  data.timestamp = 0.01;
  synchro.addSensorData(SensorType::SHORT_RANGE_RADAR, data);
  output = synchro.synchronize();
  // finally we'got one synchro
  EXPECT_EQ(output.size(), 1);
  // timestamp of the output data == timestamp of the main sensor (camera) data
  EXPECT_TRUE(output.find(0) != output.end());

  output = synchro.synchronize();
  // camera data was removed -> again no synchro
  EXPECT_EQ(output.size(), 0);

  data.timestamp = 0.08;
  synchro.addSensorData(SensorType::CAMERA, data);
  output = synchro.synchronize();
  // now camera cannot synchronize with too old long range
  EXPECT_EQ(output.size(), 0);

  data.timestamp = 0.07;
  synchro.addSensorData(SensorType::LONG_RANGE_RADAR, data);
  output = synchro.synchronize();
  // now one more synchro should be there
  EXPECT_EQ(output.size(), 1);
}

/// Tests Synchronizer correctness with removing secondary matches
TEST(SensorSynchronizerTest, SensorSynchronizer_RemovingMatches)
{
  SensorSynchronizer synchro(0.05, 1, true);
  test_synchro(synchro);
}

///// Tests Synchronizer correctness without removing secondary matches
//TEST(SensorSynchronizerTest, SensorSynchronizer_KeepingMatches)
//{
//  SensorSynchronizer synchro(0.05, 1, false);
//  test_synchro(synchro);
//}


// Run all the tests that were declared with TEST()
int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
