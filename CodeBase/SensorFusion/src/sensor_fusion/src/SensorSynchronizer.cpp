#include <vector>
#include "sensor_fusion/SensorSynchronizer.h"

namespace sensor_fusion
{

/// Structure keeping timestamps of main and secondary sensors (so they could be found from unordered_map)
/// as well as type of the secondary sensor
/// Required to run O(N Log N) Djikstra data association algorithm
struct SynchroTriplet
{
  timestamp_t camera_timestamp;
  timestamp_t short_range_timestamp;
  timestamp_t long_range_timestamp;
  float delta;
};

SensorSynchronizer::SensorSynchronizer(timestamp_t max_timestamp_delta,
                                       timestamp_t history_time_limit,
                                       bool delete_secondary_matches) :
    max_timestamp_delta_(max_timestamp_delta),
    history_time_limit_(history_time_limit),
    delete_secondary_matches_(delete_secondary_matches)
{

}

void SensorSynchronizer::addSensorData(const SensorType type, const SensorData &data)
{
  clean_history(data.timestamp);
  if (type == SensorType::SHORT_RANGE_RADAR) {
    short_range_data_[data.timestamp] = data;
  } else if (type == SensorType::LONG_RANGE_RADAR) {
    long_range_data_[data.timestamp] = data;
  } else {
    camera_data_[data.timestamp] = data;
  }
}

std::map<timestamp_t, std::unordered_map<SensorType, SensorData>> SensorSynchronizer::synchronize()
{
  std::map<timestamp_t, std::unordered_map<SensorType, SensorData>> output;
  std::unordered_map<SensorType, SensorData> output_item;
  std::vector<SynchroTriplet> tuples;

  if (!camera_data_.empty()) {
    SynchroTriplet tuple {0,0,0,0};

    // go over observations collected with main_sensor
    for (auto &iter_main : camera_data_) {
      tuple.camera_timestamp = iter_main.second.timestamp;

      for (auto &iter_short : short_range_data_) {
        timestamp_t delta_short = std::fabs(iter_short.second.timestamp - tuple.camera_timestamp);

        if (delta_short <= max_timestamp_delta_) {
          tuple.delta = delta_short;
          tuple.short_range_timestamp = iter_short.second.timestamp;

          for (auto &iter_long : long_range_data_) {
            timestamp_t delta_long = std::fabs(iter_long.second.timestamp - tuple.camera_timestamp);

            if (delta_long <= max_timestamp_delta_) {
              tuple.delta *= delta_long;
              tuple.long_range_timestamp = iter_long.second.timestamp;
              tuples.push_back(tuple);
            }
          }
        }
      }
    }

    std::sort(tuples.begin(), tuples.end(), [](const SynchroTriplet &a, const SynchroTriplet &b) {
      return a.delta < b.delta;
    });

    for (size_t i = 0; i < tuples.size() && !camera_data_.empty(); i++) {
      auto camera_iter = camera_data_.find(tuples[i].camera_timestamp);
      if (camera_iter != camera_data_.end()) {
        output_item[SensorType::CAMERA] = std::move(camera_iter->second);

        auto short_range_iter = short_range_data_.find(tuples[i].short_range_timestamp);
        if (short_range_iter != short_range_data_.end()) {
          output_item[SensorType::SHORT_RANGE_RADAR] = short_range_iter->second;

          auto long_range_iter = long_range_data_.find(tuples[i].long_range_timestamp);
          if (long_range_iter != long_range_data_.end()) {
            output_item[SensorType::LONG_RANGE_RADAR] = long_range_iter->second;
            output[tuples[i].camera_timestamp] = output_item;

            camera_data_.erase(camera_iter);
            if (delete_secondary_matches_) {
              short_range_data_.erase(short_range_iter);
              long_range_data_.erase(long_range_iter);
            }
          }
        }
      }
    }
  }
  return output;
}

void SensorSynchronizer::clean_history(timestamp_t latest)
{
  clean_history(camera_data_, latest);
  clean_history(short_range_data_, latest);
  clean_history(long_range_data_, latest);
}

void SensorSynchronizer::clean_history(std::unordered_map<timestamp_t, SensorData> &data, timestamp_t latest)
{
  for (auto iter = data.begin(); iter != data.end();) {
    if (iter->first < latest - history_time_limit_) {
      iter = data.erase(iter);
    } else {
      iter++;
    }
  }
}
}
