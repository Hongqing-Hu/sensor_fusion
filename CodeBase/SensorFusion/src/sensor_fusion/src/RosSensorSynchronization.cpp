#include "sensor_fusion/RosSensorSynchronization.h"

namespace sensor_fusion
{

RosSensorSynchronization::RosSensorSynchronization(ros::Publisher ros_publisher) : BaseSensorSynchronization()
{
  ros_sender_ = ros_publisher;
}

void RosSensorSynchronization::PostFusion(timestamp_t time_stamp)
{
  if (!fusion_output_.sensor_list_data.empty()) {
    ros_sender_.publish(fusion_output_);
  }
}

}