#include "sensor_fusion/RVizMarker.h"

namespace sensor_fusion
{

ShapeMarkerCreator::ShapeMarkerCreator(unsigned int type, float red, float green, float blue, float alpha) : red_(
    red), green_(green), blue_(blue), alpha_(alpha)
{

  marker_.type = type;

  marker_.action = visualization_msgs::Marker::ADD;

  marker_.pose.position.z = 0;
  marker_.pose.orientation.x = 0.0;
  marker_.pose.orientation.y = 0.0;
  marker_.pose.orientation.z = 0.0;
  marker_.pose.orientation.w = 1.0;

  marker_.scale.x = 1.0;
  marker_.scale.y = 1.0;
  marker_.scale.z = 1.0;

  marker_.color.r = red_;
  marker_.color.g = green_;
  marker_.color.b = blue_;
  marker_.color.a = alpha_;
  marker_.lifetime = ros::Duration(1);

}

const visualization_msgs::Marker &
ShapeMarkerCreator::CreatePositionMarker(const std::string &frame_id,
                                         const std::string &shape_namespace,
                                         double x_position,
                                         double y_position)
{
  marker_.header.stamp = ros::Time::now();
  marker_.header.frame_id = frame_id;
  marker_.ns = shape_namespace;
  marker_.id = count_markers++;
  marker_.pose.position.x = x_position;
  marker_.pose.position.y = y_position;
  return marker_;
}

const visualization_msgs::Marker &
RVizMarker::ResetMarker(const std::string &frame_id, const std::string &shape_namespace)
{
  draw_reset_marker_.header.frame_id = frame_id;
  draw_reset_marker_.header.stamp = ros::Time();
  draw_reset_marker_.ns = shape_namespace;
  draw_reset_marker_.action = 3;
  draw_reset_marker_.pose.orientation.w = 1;
  return draw_reset_marker_;
}

const visualization_msgs::Marker &
RVizMarker::CreateTextMarker(const std::string &frame_id, const std::string &shape_namespace,
                             const std::string &text_show)
{
  draw_textmarker_.header.frame_id = frame_id;
  draw_textmarker_.header.stamp = ros::Time::now();
  draw_textmarker_.ns = shape_namespace;
  draw_textmarker_.id = 0;
  draw_textmarker_.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
  draw_textmarker_.action = visualization_msgs::Marker::ADD;
  draw_textmarker_.pose.position.x = 0;
  draw_textmarker_.pose.position.y = 0;
  draw_textmarker_.pose.position.z = 0;
  draw_textmarker_.pose.orientation.x = 0.0;
  draw_textmarker_.pose.orientation.y = 0.0;
  draw_textmarker_.pose.orientation.z = 0.0;
  draw_textmarker_.pose.orientation.w = 1.0;
  draw_textmarker_.scale.x = 1.0;
  draw_textmarker_.scale.y = 1.0;
  draw_textmarker_.scale.z = 1.0;
  draw_textmarker_.color.r = 0.0;
  draw_textmarker_.color.g = 1.0;
  draw_textmarker_.color.b = 1.0;
  draw_textmarker_.color.a = 1.0;
  draw_textmarker_.lifetime = ros::Duration();
  draw_textmarker_.text = text_show;
  return draw_textmarker_;
}

}