#include "sensor_fusion/SensorFaultChecker.h"

using namespace sensor_fusion;

SensorFaultChecker::SensorFaultChecker(timestamp_t sensor_fault_delay) :
  sensor_fault_delay_(sensor_fault_delay)
{}


void SensorFaultChecker::update_camera(timestamp_t latest)
{
  camera_initialized_ = true;
  camera_latest_ = latest;
  global_latest_ = std::max(global_latest_, latest);
}

void SensorFaultChecker::update_long_range(timestamp_t latest)
{
  long_range_initialized_ = true;
  long_range_latest_ = latest;
  global_latest_ = std::max(global_latest_, latest);
}

void SensorFaultChecker::update_short_range(timestamp_t latest)
{
  short_range_initialized_ = true;
  short_range_latest_ = latest;
  global_latest_ = std::max(global_latest_, latest);
}

bool SensorFaultChecker::isFault()
{
  if (camera_initialized_ && long_range_initialized_ && short_range_initialized_) {
    if (global_latest_ - camera_latest_ > sensor_fault_delay_) {
      return true;
    }

    if (global_latest_ - long_range_latest_ > sensor_fault_delay_) {
      return true;
    }

    if (global_latest_ - short_range_latest_ > sensor_fault_delay_) {
      return true;
    }
  }
  return false;
}
