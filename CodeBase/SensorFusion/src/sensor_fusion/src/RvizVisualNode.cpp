#include <ros/ros.h>
#include <visualization_msgs/MarkerArray.h>
#include "sensor_fusion/RVizMarker.h"
#include "sensor_fusion/SensorData.h"

using namespace sensor_fusion;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "visual_sensor_fusion_node");

  ros::NodeHandle params("~");

  std::string input_topic, output_topic;
  int shape;
  float red, green, blue, alpha;

  params.getParam("input_topic", input_topic);
  params.getParam("output_topic", output_topic);
  params.getParam("shape", shape);
  params.param<float>("red", red, 1.0);
  params.param<float>("green", green, 1.0);
  params.param<float>("blue", blue, 1.0);
  params.param<float>("alpha", alpha, 1.0);

  ros::NodeHandle n;

  auto publisher_ = n.advertise<visualization_msgs::MarkerArray>(output_topic, 1);

  visualization_msgs::MarkerArray marker_array_;
  RVizMarker rviz_marker_;
  ShapeMarkerCreator marker_creator_((unsigned int) shape, red, green, blue, alpha);

  auto subscriber = n.subscribe<sensor_fusion::SensorData>(input_topic, 100, [&](const SensorData::ConstPtr &message) {
    marker_array_.markers.push_back(rviz_marker_.ResetMarker("/my_frame", "sensor_fusion"));
    marker_array_.markers.push_back(rviz_marker_.CreateTextMarker("/my_frame", "sensor_fusion", "Car_Reference"));

    std::vector<SensorPoint> points = message->sensor_list_data;
    for (auto &point : points) {
      ROS_INFO("Topic name: %s x_position_ [%f] y_position_ [%f]", input_topic.c_str(),
               point.x_position, point.y_position);
      marker_array_.markers.push_back(marker_creator_.CreatePositionMarker("/my_frame",
                                                                           "sensor_fusion",
                                                                           point.x_position,
                                                                           point.y_position));
    }

    publisher_.publish(marker_array_);
    marker_array_.markers.clear();
  });

  ros::spin();

  return 0;

}
