#include <utility>
#include <stdexcept>
#include "sensor_fusion/SensorFusion.h"

namespace sensor_fusion
{

/// Here we use unsigned integers because indexes may never be negative
struct FusionPair
{
  size_t camera_index;
  size_t secondary_index;
  SensorType secondary_type;
  float distance;
};

/// Euclidean distance between object centers
float distance(const SensorPoint& a, const SensorPoint& b)
{
  float diff = (a.x_position - b.x_position) * (a.x_position - b.x_position);
  diff += (a.y_position - b.y_position) * (a.y_position - b.y_position);
  return std::sqrt(diff);
}

SensorFusion::SensorFusion(const float distance_threshold) :
    distance_threshold_(distance_threshold)
{

}


SensorData SensorFusion::fuse(const std::unordered_map<SensorType, SensorData> &input)
{
  if(input.find(SensorType::CAMERA) == input.end() ||
     input.find(SensorType::SHORT_RANGE_RADAR) == input.end() ||
     input.find(SensorType::LONG_RANGE_RADAR) == input.end()) {
    throw std::runtime_error("SensorFusion: not all sensors inputs are given");
  }

  const std::vector<SensorPoint> camera_data = input.at(SensorType::CAMERA).sensor_list_data;
  const std::vector<SensorPoint> short_range_data = input.at(SensorType::SHORT_RANGE_RADAR).sensor_list_data;
  const std::vector<SensorPoint> long_range_data = input.at(SensorType::LONG_RANGE_RADAR).sensor_list_data;

  SensorData output;
  output.timestamp = input.at(SensorType::CAMERA).timestamp;
  SensorPoint output_point;

  std::vector<FusionPair> pairs;

  // Here we use Fast Djikstra algorithm for grouping observations together
  if (!camera_data.empty()) {
    FusionPair pair;

    // go over observations collected with main_sensor
    for (size_t i = 0; i < camera_data.size(); i++) {
      pair.camera_index = i;
      const SensorPoint camera_object = camera_data[i];

      // collect pairs between camera & short-range
      for (size_t j = 0; j < short_range_data.size(); j++) {
        const SensorPoint short_range_object = short_range_data[j];

        float short_distance = distance(camera_object, short_range_object);

        if (short_distance <= distance_threshold_) {
          pair.distance = short_distance;
          pair.secondary_index = j;
          pair.secondary_type = SensorType::SHORT_RANGE_RADAR;
          pairs.push_back(pair);
        }
      }

      // collect pairs between camera & short-range
      for (size_t j = 0; j < long_range_data.size(); j++) {
        const SensorPoint long_range_object = long_range_data[j];

        float long_distance = distance(camera_object, long_range_object);

        if (long_distance <= distance_threshold_) {
          pair.distance = long_distance;
          pair.secondary_index = j;
          pair.secondary_type = SensorType::LONG_RANGE_RADAR;
          pairs.push_back(pair);
        }
      }
    }

    std::sort(pairs.begin(), pairs.end(), [](const FusionPair &a, const FusionPair &b) {
      return a.distance < b.distance;
    });

    // hash-table for storing already assigned elements
    // we must never re-use them again
    std::unordered_map<size_t, bool> camera_assigned;
    std::unordered_map<SensorType, std::unordered_map<size_t, bool>> secondary_assigned;

    for (size_t i = 0; i < pairs.size(); i++) {
      const bool camera_used = camera_assigned[pairs[i].camera_index];
      const bool secondary_used = secondary_assigned[pairs[i].secondary_type][pairs[i].secondary_index];

      // should not proceed if any of elements was used before
      if(camera_used || secondary_used)
      {
        continue;
      }

      // Take output point from the camera data
      // It is also possible to do averaging of coordinates, instead
      output_point = camera_data[pairs[i].camera_index];
      output.sensor_list_data.push_back(output_point);
      camera_assigned[pairs[i].camera_index] = true;
      secondary_assigned[pairs[i].secondary_type][pairs[i].secondary_index] = true;
    }
  }

  return output;
}
}
