#include "sensor_fusion/BaseSensorSynchronization.h"
#include "sensor_fusion/IFusionStrategy.h"

namespace sensor_fusion
{

BaseSensorSynchronization::BaseSensorSynchronization() = default;

void BaseSensorSynchronization::Process(SensorType sensor, const SensorData &msg)
{
  PrepareData(sensor, msg);
  auto time_stamp = GetSynchronizedTimeStamp();
  if (time_stamp >= 0) {
    PreFusion(time_stamp);
    SynchronizeSensorDataMsg(time_stamp);
    Fuse(time_stamp);
    PostFusion(time_stamp);
    RemoveSensorDataMsg(time_stamp);
    sync_time_stamp_ = time_stamp;
  }
}

void BaseSensorSynchronization::PrepareData(SensorType sensor, const SensorData &msg)
{
  sensor_data_[sensor].push_back(msg);
}

timestamp_t BaseSensorSynchronization::GetLatestTimeStamp(SensorType sensor)
{
  if (sensor_data_[sensor].empty())
    return -1;
  else {
    return sensor_data_[sensor].back().timestamp;
  }
}

timestamp_t BaseSensorSynchronization::GetEarliestTimeStamp(SensorType sensor)
{
  if (sensor_data_[sensor].empty())
    return -1;
  else {
    return sensor_data_[sensor].front().timestamp;
  }
}

timestamp_t BaseSensorSynchronization::GetSynchronizedTimeStamp()
{
  auto t1 = GetEarliestTimeStamp(CAMERA);
  auto t2 = GetLatestTimeStamp(SHORT_RANGE_RADAR);
  auto t3 = GetLatestTimeStamp(LONG_RANGE_RADAR);
  return (t1 <= t2 && t1 <= t3) ? t1 : -1;
}

void BaseSensorSynchronization::RemoveSensorDataMsg(timestamp_t time_stamp)
{
  for (auto &msg_list : sensor_data_) {
    msg_list.remove_if([time_stamp](SensorData &n) { return n.timestamp <= time_stamp; });
  }
}

void BaseSensorSynchronization::SynchronizeSensorDataMsg(timestamp_t time_stamp)
{
  for (unsigned int sensor = FIRST_SENSOR; sensor < NUM_SENSORS; ++sensor) {
    for (const auto &sensor_data : sensor_data_[sensor]) {
      if (sensor == CAMERA) {
        fusion_input_.push_front(sensor_data);
      } else {
        fusion_input_.push_back(sensor_data);
      }
    }
  }
}

void BaseSensorSynchronization::Fuse(timestamp_t time_stamp)
{
  algo_->algorithm(fusion_input_, fusion_output_);
}

}