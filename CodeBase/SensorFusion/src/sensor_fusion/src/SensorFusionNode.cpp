#include "sensor_fusion/SensorData.h"
#include "sensor_fusion/CsvInputAdaptor.h"
#include "sensor_fusion/RosSensorSynchronization.h"
#include "sensor_fusion/SlowFusionStrategy.h"

using namespace sensor_fusion;

ros::Subscriber subscribe(ros::NodeHandle &n, std::shared_ptr<RosSensorSynchronization> fusion,
                          const std::string &topic,
                          SensorType sensor)
{
  return n.subscribe<SensorData>(topic, 100, [=](const SensorData::ConstPtr &msg) {
    try {
      fusion->Process(sensor, *msg);
    } catch (...) {
      ROS_ERROR("Fusion error");
    }
  });
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "FusionNode");
  ros::NodeHandle n;
  ros::NodeHandle nh("~");
  std::string output_topic = nh.param<std::string>("output_topic", "SensorFusionNode");
  std::string camera_input_topic = nh.param<std::string>("camera_input_topic", "CameraSensor");
  std::string long_range_input_topic = nh.param<std::string>("long_range_input_topic", "LongRangeSensor");
  std::string short_range_input_topic = nh.param<std::string>("short_range_input_topic", "ShortRangeSensor");

  auto publisher = n.advertise<SensorData>(output_topic, 100);

  auto fusion = std::make_shared<RosSensorSynchronization>(publisher);
  auto algo = std::make_shared<sensor_fusion::SlowFusionStrategy>(0.5);
  fusion->SetFusionStrategy(algo);

  auto camera_subscriber = subscribe(n, fusion, camera_input_topic, CAMERA);
  auto srr_subscriber = subscribe(n, fusion, short_range_input_topic, SHORT_RANGE_RADAR);
  auto lrr_subscriber = subscribe(n, fusion, long_range_input_topic, LONG_RANGE_RADAR);

  ros::spin();

  return 0;
}
