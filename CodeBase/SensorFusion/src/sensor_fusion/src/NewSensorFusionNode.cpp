#include <ros/ros.h>
#include <sensor_fusion/SensorFaultChecker.h>
#include "sensor_fusion/SensorData.h"
#include "sensor_fusion/SensorSynchronizer.h"
#include "sensor_fusion/SensorFusion.h"

using namespace sensor_fusion;



int main(int argc, char **argv)
{
  ros::init(argc, argv, "SynchronizerNode");
  ros::NodeHandle n;
  ros::NodeHandle nh("~");
  const std::string output_topic = nh.param<std::string>("output_topic", "SensorFusionNode");
  const std::string camera_input_topic = nh.param<std::string>("camera_input_topic", "CameraSensor");
  const std::string long_range_input_topic = nh.param<std::string>("long_range_input_topic", "LongRangeSensor");
  const std::string short_range_input_topic = nh.param<std::string>("short_range_input_topic", "ShortRangeSensor");
  const int msg_queue_size = nh.param<int>("msg_queue_size", 10);
  const timestamp_t synchronization_max_time_delta = nh.param<timestamp_t>("synchronization_max_time_delta", 0.05);
  const timestamp_t sensor_fault_delay = nh.param<timestamp_t>("sensor_fault_delay", 100);
  const float fusion_max_distance = nh.param<float>("fusion_max_distance", 0.5);

  auto publisher = n.advertise<SensorData>(output_topic, msg_queue_size);

  SensorFaultChecker fail(sensor_fault_delay);

  SensorSynchronizer synchro(synchronization_max_time_delta, synchronization_max_time_delta * 10, true);

  SensorFusion fusion(fusion_max_distance);

  auto srr_subscriber =
      n.subscribe<SensorData>(short_range_input_topic, msg_queue_size, [&](const SensorData::ConstPtr &msg) {
        synchro.addSensorData(SensorType::SHORT_RANGE_RADAR, *msg);
        fail.update_short_range(msg->timestamp);
        if (fail.isFault()) {
          ROS_ERROR("SENSOR FAIL DETECTED");
        }
      });

  auto lrr_subscriber =
      n.subscribe<SensorData>(long_range_input_topic, msg_queue_size, [&](const SensorData::ConstPtr &msg) {
        synchro.addSensorData(SensorType::LONG_RANGE_RADAR, *msg);
        fail.update_long_range(msg->timestamp);
        if (fail.isFault()) {
          ROS_ERROR("SENSOR FAIL DETECTED");
        }
      });

  auto camera_subscriber =
      n.subscribe<SensorData>(camera_input_topic, msg_queue_size, [&](const SensorData::ConstPtr &msg) {
        auto list = synchro.synchronize();

        // there is possibility that more than one item will be returned
        for (auto iter = list.begin(); iter != list.end(); iter++) {
          auto output = fusion.fuse(iter->second);
          publisher.publish(output);
        }
        synchro.addSensorData(SensorType::CAMERA, *msg);

        fail.update_camera(msg->timestamp);
        if (fail.isFault()) {
          ROS_ERROR("SENSOR FAIL DETECTED");
        }

      });

  ros::spin();

  return 0;
}
