#include "sensor_fusion/CsvInputAdaptor.h"
#include <boost/tokenizer.hpp>
#include <fstream>

using namespace sensor_fusion;

namespace sensor_fusion
{

CsvInputAdaptor::CsvInputAdaptor(const std::string &source)
    : filename_(source)
{}

std::unique_ptr<std::list<SensorData>> CsvInputAdaptor::parseFile()
{
  typedef boost::tokenizer<boost::escaped_list_separator<char>> Tokenizer;

  std::string data(filename_);

  std::ifstream file(data.c_str());

  auto sensor_data_list = std::make_unique<std::list<SensorData>>();
  timestamp_t current_timestamp = std::numeric_limits<timestamp_t>::min();

  std::string line;
  getline(file, line);
  while (getline(file, line)) {
    std::vector<std::string> vec;
    Tokenizer tok(line);
    vec.assign(tok.begin(), tok.end());
    SetSensorData(vec, current_timestamp, *sensor_data_list);
  }
  return std::move(sensor_data_list);
}

void CsvInputAdaptor::SetSensorData(const std::vector<std::string> &csv_data,
                                    timestamp_t &current_timestamp,
                                    std::list<sensor_fusion::SensorData> &sensor_data_list)
{
  if (csv_data.size() == data_size_valid) {
    SensorPoint sensor_point;

    std::stringstream timestamp_convert(csv_data[csv_timestamp_position]);
    uint64_t timestamp;
    timestamp_convert >> timestamp;

    std::stringstream object_id_convert(csv_data[csv_object_id_position]);
    object_id_convert >> sensor_point.object_id;

    std::stringstream x_value_convert(csv_data[csv_x_position]);
    x_value_convert >> sensor_point.x_position;

    std::stringstream y_value_convert(csv_data[csv_y_position]);
    y_value_convert >> sensor_point.y_position;

    if (timestamp != current_timestamp) {
      current_timestamp = timestamp;

      sensor_data_list.emplace_back();
      sensor_data_list.back().timestamp = current_timestamp;
    }

    sensor_data_list.back().sensor_list_data.push_back(sensor_point);
  }
}
}  