#include "sensor_fusion/SlowFusionStrategy.h"

namespace sensor_fusion
{
static void ResetSensorDataMsg(SensorData &msg)
{
  msg.timestamp = (uint64_t) -1;
  msg.sensor_list_data.clear();
}

static double GetDistance(const SensorPoint &p1, const SensorPoint &p2)
{
  return sqrt((p1.x_position - p2.x_position) * (p1.x_position - p2.x_position) +
      (p1.y_position - p2.y_position) * (p1.y_position - p2.y_position));
}

void SlowFusionStrategy::algorithm(const std::list<SensorData> &fusion_in, SensorData &fusion_out)
{
  if (fusion_in.empty()) {
    throw std::runtime_error("SlowFusionStrategy::algorithm(.): empty input list");
  }
  if (fusion_in.size() < 2) {
    throw std::runtime_error("SlowFusionStrategy::algorithm(.): too few input SensorData frames");
  }

  ResetSensorDataMsg(fusion_out);
  auto main_sensor_data_msg = fusion_in.front();
  auto iter = fusion_in.begin();
  for (std::advance(iter, 1); iter != fusion_in.end(); ++iter) {
    auto selected_main_sensor_point_index = -1;
    auto selected_ref_sensor_point_index = -1;
    double min_distance = tolerance_;
    auto selected_iter = iter;
    for (auto i = 0; i < main_sensor_data_msg.sensor_list_data.size(); i++) {
      for (auto j = 0; j < iter->sensor_list_data.size(); j++) {
        auto distance = GetDistance(main_sensor_data_msg.sensor_list_data[i], iter->sensor_list_data[j]);
        if (distance <= min_distance) {
          selected_main_sensor_point_index = i;
          selected_ref_sensor_point_index = j;
          selected_iter = iter;
          min_distance = distance;
        }
      }
      if (selected_main_sensor_point_index >= 0) {
        fusion_out.sensor_list_data.push_back(
            main_sensor_data_msg.sensor_list_data[selected_main_sensor_point_index]);
        selected_main_sensor_point_index = -1;
        selected_ref_sensor_point_index = -1;
        min_distance = tolerance_;
      }
    }
  }
  if (!fusion_out.sensor_list_data.empty()) {
    fusion_out.timestamp = main_sensor_data_msg.timestamp;
  }
  return;
}

} // namespace sensor_fusion