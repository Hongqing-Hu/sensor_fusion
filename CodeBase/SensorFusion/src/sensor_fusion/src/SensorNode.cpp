#include "sensor_fusion/InputAdaptor.h"
#include "sensor_fusion/CsvInputAdaptor.h"
#include <ros/ros.h>
#include <std_msgs/String.h>

using namespace sensor_fusion;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "SensorNode");
  ros::NodeHandle nh("~");
  std::string csv_filename, topic_name;
  int rate;
  if (!nh.getParam("csv_filename", csv_filename)) {
    ROS_ERROR("'csv_filename' parameter is not specified!");
    return 1;
  }
  if (!nh.getParam("topic_name", topic_name)) {
    ROS_ERROR("'topic_name' parameter is not specified!");
    return 1;
  }
  nh.param<int>("rate", rate, 1);

  CsvInputAdaptor csv_reader(csv_filename);

  ros::NodeHandle n;
  auto publisher = n.advertise<SensorData>(topic_name, 100);

  ros::Rate loop_rate(rate);
  auto sensor_data_list = csv_reader.parseFile();
  for (const auto &sensor_data : *sensor_data_list) {
    publisher.publish(sensor_data);
    loop_rate.sleep();
  }
  ros::spin();

  return 0;
}
