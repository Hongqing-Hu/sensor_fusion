#ifndef _CSV_INPUT_ADAPTER_
#define _CSV_INPUT_ADAPTER_

#include "sensor_fusion/SensorData.h"
#include "InputAdaptor.h"
#include <vector>

namespace sensor_fusion
{
using timestamp_t = SensorData::_timestamp_type;

class CsvInputAdaptor : public IInputAdaptor
{
 public:
  explicit CsvInputAdaptor(const std::string &source);
  ~CsvInputAdaptor() override = default;

  std::unique_ptr<std::list<SensorData>> parseFile() override;
 private:
  static void SetSensorData(const std::vector<std::string> &csv_data,
                            timestamp_t &current_timestamp,
                            std::list<SensorData> &sensor_data_list);

  std::string filename_;
  static const unsigned data_size_valid = 4;
  static const unsigned csv_timestamp_position = 3;
  static const unsigned csv_object_id_position = 0;
  static const unsigned csv_x_position = 1;
  static const unsigned csv_y_position = 2;
};

}

#endif // _CSV_INPUT_ADAPTER_
