#ifndef _ADAPTOR_H_
#define _ADAPTOR_H_

#include <list>
#include <string>
#include <memory>
#include "sensor_fusion/SensorData.h"

namespace sensor_fusion
{

class IInputAdaptor
{
 public:
  IInputAdaptor() = default;
  virtual ~IInputAdaptor() = default;
  virtual std::unique_ptr<std::list<SensorData>> parseFile() = 0;
};

}

#endif  //_ADAPTOR_H_