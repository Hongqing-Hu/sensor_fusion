#ifndef _DEMOEXECUTESENSORFUSION_H_
#define _DEMOEXECUTESENSORFUSION_H_

#include "BaseSensorSynchronization.h"
#include <ros/ros.h>
#include <exception>

namespace sensor_fusion
{
using timestamp_t = SensorData::_timestamp_type;

class RosSensorSynchronization : public BaseSensorSynchronization
{
 public:
  RosSensorSynchronization(ros::Publisher);
  void PostFusion(timestamp_t time_stamp) override;
  ros::Publisher ros_sender_;
};

}

#endif // _DEMOEXECUTESENSORFUSION_H_
