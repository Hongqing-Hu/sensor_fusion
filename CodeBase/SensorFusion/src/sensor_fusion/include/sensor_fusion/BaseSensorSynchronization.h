#ifndef _IEXECUTESENSORFUSION_H_
#define _IEXECUTESENSORFUSION_H_

#include <string>
#include <memory>
#include <array>
#include "sensor_fusion/SensorData.h"

namespace sensor_fusion
{
class IFusionStrategy;

enum SensorType : unsigned int
{
  FIRST_SENSOR = 0,
  CAMERA = FIRST_SENSOR,
  SHORT_RANGE_RADAR,
  LONG_RANGE_RADAR,
  NUM_SENSORS
};

using timestamp_t = SensorData::_timestamp_type;

class BaseSensorSynchronization
{
 public:
  BaseSensorSynchronization();
  virtual ~BaseSensorSynchronization() = default;

  void Process(SensorType sensor, const SensorData &msg);
  void SetFusionStrategy(std::shared_ptr<IFusionStrategy> algo)
  { algo_ = algo; }

 protected:
  std::array<std::list<sensor_fusion::SensorData>, NUM_SENSORS> sensor_data_;
  std::list<SensorData> fusion_input_;
  SensorData fusion_output_;
  timestamp_t sync_time_stamp_{-1};

 private:
  std::shared_ptr<IFusionStrategy> algo_;
  virtual void PrepareData(SensorType sensor, const SensorData &sensor_data);
  virtual void PreFusion(timestamp_t time_stamp)
  {}
  virtual void Fuse(timestamp_t time_stamp);
  virtual void PostFusion(timestamp_t time_stamp)
  {}
  void RemoveSensorDataMsg(timestamp_t time_stampe);
  timestamp_t GetSynchronizedTimeStamp(void);
  void SynchronizeSensorDataMsg(timestamp_t time_stamp);
  timestamp_t GetLatestTimeStamp(SensorType sensor);
  timestamp_t GetEarliestTimeStamp(SensorType sensor);
};

}

#endif // _IEXECUTESENSORFUSION_H_
