#ifndef SENSOR_FUSION_SENSORFAULTCHECKER_H
#define SENSOR_FUSION_SENSORFAULTCHECKER_H

#include "sensor_fusion/SensorData.h"

namespace sensor_fusion
{
/// timestamp is measured in seconds
using timestamp_t = SensorData::_timestamp_type;

// Checks if some sensor have not submitted for too long
class SensorFaultChecker
{
 public:
  /// @param sensor_fault_delay silence time threshold for considering sensor to be faulty
  SensorFaultChecker(timestamp_t sensor_fault_delay);

  void update_camera(timestamp_t latest);
  void update_long_range(timestamp_t latest);
  void update_short_range(timestamp_t latest);
  bool isFault();

 private:
  timestamp_t sensor_fault_delay_;
  timestamp_t global_latest_;
  timestamp_t camera_latest_;
  timestamp_t long_range_latest_;
  timestamp_t short_range_latest_;

  bool camera_initialized_ = false;
  bool long_range_initialized_ = false;
  bool short_range_initialized_ = false;

};
}
#endif //SENSOR_FUSION_SENSORFAULTCHECKER_H
