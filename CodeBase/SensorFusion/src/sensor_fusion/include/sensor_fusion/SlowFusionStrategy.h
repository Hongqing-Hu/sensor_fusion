#ifndef _SLOWFUSIONSTRATEGY_H_
#define _SLOWFUSIONSTRATEGY_H_

#include "IFusionStrategy.h"
#include <vector>
#include "sensor_fusion/SensorData.h"
#include "sensor_fusion/SensorPoint.h"

namespace sensor_fusion
{

class SlowFusionStrategy : public IFusionStrategy
{
 public:
  SlowFusionStrategy(double tolerance) : IFusionStrategy(tolerance)
  {}

  void algorithm(const std::list<SensorData> &fusion_in, SensorData &fusion_out) override;
};

}
#endif // _SLOWFUSIONSTRATEGY_H_ 