#ifndef _IFUSIONSTRATEGY_H_
#define _IFUSIONSTRATEGY_H_

#include "sensor_fusion/SensorData.h"

namespace sensor_fusion
{

class IFusionStrategy
{
 public:
  IFusionStrategy(double tolerance) : tolerance_(tolerance)
  {}
  virtual void algorithm(const std::list<SensorData> &fusion_in, SensorData &fusion_out) = 0;

 protected:
  double tolerance_{0.5};
};

};

#endif // _IFUSIONSTRATEGY_H_

