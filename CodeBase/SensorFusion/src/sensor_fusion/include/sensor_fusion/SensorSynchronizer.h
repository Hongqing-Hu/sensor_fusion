/// SensorSynchronizer.cpp

#ifndef SENSOR_FUSION_SENSORSYNCHRONIZER_H
#define SENSOR_FUSION_SENSORSYNCHRONIZER_H

#include <unordered_map>
#include "sensor_fusion/SensorData.h"

namespace sensor_fusion
{

enum class SensorType : unsigned int
{
  FIRST_SENSOR = 0,
  CAMERA = FIRST_SENSOR,
  SHORT_RANGE_RADAR,
  LONG_RANGE_RADAR,
  NUM_SENSORS
};

/// timestamp is measured in seconds
using timestamp_t = SensorData::_timestamp_type;

/// Algorithm for time-synchronization of arbitrary number of Sensors
/// Adding new data-frame (one sensor reading) is decoupled from actual synchronization process
// It's written in very generic way, so it does not rely too much on any assumption
// Sensors may have significantly different frame-rates
//
// Main sensor should be specified in
class SensorSynchronizer
{
 public:

  /// Creates instance of SensorSynchronizer
  /// @param max_time_delta - maximum time difference between different sensor readings,
  /// when they still can be considered as potential match
  /// @param history_time_limit - keeps history
  /// @param main_sensor - provides what sensor taken as main one
  /// historical records of the main sensor are always deleted from the internal buffer,
  /// after they were synchronized and returned back by synchronize()
  SensorSynchronizer(timestamp_t max_time_delta = 0.05,
                     timestamp_t history_time_limit = 1,
                     bool delete_secondary_matches = true);

  /// Adds new sensor observation into internal buffer
  void addSensorData(SensorType type, const SensorData &data);

  /// performs synchronization
  std::map<timestamp_t, std::unordered_map<SensorType, SensorData>> synchronize();

 private:

  std::unordered_map<timestamp_t, SensorData> camera_data_;
  std::unordered_map<timestamp_t, SensorData> short_range_data_;
  std::unordered_map<timestamp_t, SensorData> long_range_data_;

  bool delete_secondary_matches_;

  /// controls
  const timestamp_t max_timestamp_delta_;

  /// private parameters controlling
  const timestamp_t history_time_limit_;

  /// cleans up too ancient history
  void clean_history(timestamp_t latest);

  void clean_history(std::unordered_map<timestamp_t, SensorData> &, timestamp_t latest);
};

}

#endif //SENSOR_FUSION_SENSORSYNCHRONIZER_H
