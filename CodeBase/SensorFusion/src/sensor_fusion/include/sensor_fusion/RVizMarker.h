#ifndef _RVIZ_MARKER_H_
#define _RVIZ_MARKER_H_

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <sensor_msgs/Range.h>

namespace sensor_fusion
{

class ShapeMarkerCreator
{
 public:
  ShapeMarkerCreator(unsigned int type, float red, float green, float blue, float alpha);

  const visualization_msgs::Marker &CreatePositionMarker(const std::string &frame_id,
                                                         const std::string &shape_namespace,
                                                         double x_position,
                                                         double y_position);
 private:
  int count_markers = 0;
  float red_, green_, blue_, alpha_;
  visualization_msgs::Marker marker_;
};

class RVizMarker
{
 public:
  RVizMarker() = default;
  ~RVizMarker() = default;

  const visualization_msgs::Marker &ResetMarker(const std::string &frame_id,
                                                const std::string &shape_namespace);

  const visualization_msgs::Marker &CreateTextMarker(const std::string &frame_id,
                                                     const std::string &shape_namespace,
                                                     const std::string &text_show);

 private:
  visualization_msgs::Marker draw_reset_marker_;
  visualization_msgs::Marker draw_textmarker_;

};

}

#endif //_RVIZ_MARKER_H_