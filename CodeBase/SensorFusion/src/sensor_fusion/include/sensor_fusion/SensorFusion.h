#ifndef SENSOR_FUSION_SENSORFUSION_H
#define SENSOR_FUSION_SENSORFUSION_H

#include "SensorSynchronizer.h"

namespace sensor_fusion
{

class SensorFusion
{
 public:
  SensorFusion(float distance_threshold = 0.5);

  SensorData fuse(const std::unordered_map<SensorType, SensorData> &input);
 private:
  float distance_threshold_;

};
}

#endif //SENSOR_FUSION_SENSORFUSION_H
