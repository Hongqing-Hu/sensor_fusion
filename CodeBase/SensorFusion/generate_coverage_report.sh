#!/bin/sh -ex

catkin_make
catkin_make run_tests
lcov -c -d build -o doc/report.lcov
genhtml -o doc/code_coverage doc/report.lcov
