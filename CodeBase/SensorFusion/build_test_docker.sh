#!/bin/bash

sudo docker run -it \
  -v `pwd`/src:/catkin_ws/src \
  --rm=true \
  osrf/ros:melodic-desktop-full \
  /bin/bash -c "cd /catkin_ws && source /opt/ros/melodic/setup.bash && catkin_make && ( roscore & catkin_make run_tests )"
