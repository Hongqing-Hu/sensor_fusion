# Acceptance test
## Generate basic acceptance test report

This project expects python3, pip3 and a linux environment

Place the `expectedOutputData.csv` file inside the `./data` directory

Extract the output of the sensor fusion system into a csv format according to the spec defined in the `expectedOutputData.csv` file. 

Name the system Output csv `givenOutputData.csv` and place inside the `./data` directory

Perform the following to generate a test report, comparing the system's output to the expected Output:

`pip3 install -r requirements.txt`
`$ cd test`
`$ python3 main.py 0.1`
where 0.1 is the tolerance that is passed into the acceptanceTest report generator as a parameter