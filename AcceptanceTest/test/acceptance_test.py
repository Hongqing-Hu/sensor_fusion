import argparse
import multiprocessing

import pandas as pd
from joblib import Parallel, delayed
from termcolor import colored
from tqdm import tqdm

pd.set_option('mode.chained_assignment', None)


def colored_reverse_blink(text, color):
    return colored(text, color, attrs=['reverse', 'blink'])


class TestReport:
    def __init__(self, total):
        self.total = total

    def __str__(self):
        text = ''
        error_count = 0.0
        sample_count = float(len(self.total))
        for report in self.total:
            if(report["miss"]/report["match"] > 0.1):
                text += colored_reverse_blink(' ', 'red')
                error_count += 1
            else:
                text += colored_reverse_blink(' ', 'green')
        if (error_count/sample_count) > 0.05:
            text += colored_reverse_blink('FAIL', 'red')
            text += '\nNot enough samples contain accurate readings'
        else:
            text += colored_reverse_blink('PASS', 'green')
            text += '\nSamples contain satisfactory number of accurate readings'
        return text


class AcceptanceTest:
    def __init__(self, tolerance):
        self.tolerance = tolerance

    @staticmethod
    def merge_timestamps(ts1, ts2):
        merged_timestamps = pd.concat([ts1, ts2])
        return sorted(merged_timestamps.unique().tolist())

    @staticmethod
    def filter_by_timestamp(outputs, timestamp):
        matches = outputs['timestamp'] == timestamp
        return outputs[matches]

    @staticmethod
    def any_df_empty(dataframes):
        return any(len(df) == 0 for df in dataframes)

    @staticmethod
    def euclidean_distance(df_left, df_right):
        dx = df_left["x"] - df_right["x"]
        dy = df_left["y"] - df_right["y"]
        return dx**2 + dy**2

    @staticmethod
    def find_closest(expected, given):
        min_distance = float('inf')
        for _, exp in expected.iterrows():
            for _, giv in given.iterrows():
                distance = AcceptanceTest.euclidean_distance(exp, giv)
                if distance < min_distance:
                    min_distance = distance
                    min_exp = exp
                    min_giv = giv
        return min_distance, min_exp, min_giv

    @staticmethod
    def drop_matching(dataframe, target):
        matches = (dataframe['objId'] == target["objId"]) & \
            (dataframe['x'] == target["x"]) & \
            (dataframe['y'] == target["y"]) & \
            (dataframe['timestamp'] == target["timestamp"])
        return dataframe.drop(dataframe[matches].index, inplace=False, errors="ignore")

    def _process_timestamp(self, timestamp, expected, given):
        match_count = 0
        miss_count = 0
        expected = self.filter_by_timestamp(expected, timestamp)
        given = self.filter_by_timestamp(given, timestamp)
        if self.any_df_empty([expected, given]):
            # add the unmatched outputs. if any
            miss_count += max(len(expected), len(given))
        else:
            while not self.any_df_empty([expected, given]):
                min_distance, min_exp, min_giv = self.find_closest(expected, given)
                if min_distance < self.tolerance**2:
                    match_count += 1
                else:
                    miss_count += 1
                # drop the matched values
                expected = self.drop_matching(expected, min_exp)
                given = self.drop_matching(given, min_giv)

        if self.any_df_empty([expected, given]):
            # add the unmatched outputs. if any
            miss_count += max(len(expected), len(given))
        return {"timestamp": timestamp, "match": match_count, "miss": miss_count}

    def match(self, expected_output_csv, given_output_csv):
        expected_output = pd.read_csv(expected_output_csv)
        given_output = pd.read_csv(given_output_csv)
        all_time_stamps = self.merge_timestamps(expected_output['timestamp'], given_output['timestamp'])
        num_cores = multiprocessing.cpu_count()

        @delayed
        def process(timestamp):
            return self._process_timestamp(timestamp, expected_output, given_output)
        results = Parallel(n_jobs=num_cores)(process(ts) for ts in tqdm(all_time_stamps))

        return TestReport(results)


def parse_arguments():
    parser = argparse.ArgumentParser(description="Acceptance test")
    parser.add_argument('expected', help='Expected output', default='../data/expectedOutputData.csv')
    parser.add_argument('given', help='Given output', default='../data/givenOutputData.csv')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_arguments()
    report = AcceptanceTest(tolerance=0.1).match(args.expected, args.given)
    print('')
    print(report)
