Legend:
    C : CameraData Present
    S : ShortRangeRadarData Present
    L : LongRangeRadarData Present
    E : ExpectedOutputData Present

Functional Test:
    for i in timestamps:
        Ci,Si,Li,Ei
        0, 0, 0, 0
        0, 0, 1, 0
        0, 1, 0, 0
        0, 1, 1, 0
        1, 0, 0, 0
        1, 0, 1, 1
        1, 1, 0, 1
        1, 1, 1, 1

Points of failure:
    No points observed in sensor Frame; No Output
    Frames arrive out of order;
    Frames arrive delayed;
    Sensor keeps sending the same frames over and over:
        One sensor;
        Two Sensors;
        .
        .
        .
        n Sensors;
    
Only one sensor is transmitting
Only 2 sensors are transmitting
.
.
.
.
Only n sensors are transmitting





