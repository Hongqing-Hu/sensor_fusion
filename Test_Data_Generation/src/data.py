from collections import OrderedDict
from random import randint, uniform

import pandas as pd
from tqdm import tqdm


class DataCreator:
    def __init__(self, camera_config, short_range_radar_config,
                 long_range_radar_config,
                 total_number_of_objects,
                 step,
                 timesteps):

        self.total_number_of_objects = total_number_of_objects
        self.step = step
        self.timesteps = timesteps

        self.data = []

        self.camera_config = camera_config
        self.short_range_radar_config = short_range_radar_config
        self.long_range_radar_config = long_range_radar_config
        self.sensor_configs = [self.camera_config,
                               self.short_range_radar_config,
                               self.long_range_radar_config]

        self.x_min = max(sensor.x_min for sensor in self.sensor_configs)
        self.x_max = max(sensor.x_max for sensor in self.sensor_configs)
        self.y_min = max(sensor.y_min for sensor in self.sensor_configs)
        self.y_max = max(sensor.y_max for sensor in self.sensor_configs)

    def _pointVisibleToCameraAndAnotherSensor(self, xy):
        [x, y] = xy

        if (self.camera_config.in_range(x=x, y=y) and
            (self.short_range_radar_config.in_range(x=x, y=y)
             or self.long_range_radar_config.in_range(x=x, y=y))):
            return True
        return False

    def _generateValidGroundTruthDataPoint(self, obj_id, ts):

        sensor_data_spec = OrderedDict()

        while True:

            x = uniform(float(self.x_min), float(self.x_max))
            y = uniform(float(self.y_min), float(self.y_max))
            if self._pointVisibleToCameraAndAnotherSensor(xy=[x, y]):

                sensor_data_spec["objId"] = obj_id
                sensor_data_spec["x"] = x
                sensor_data_spec["y"] = y
                sensor_data_spec["timestamp"] = ts

                return sensor_data_spec

    def generateGroundTruthData(self):

        self.data = []
        for ts in tqdm(range(0, self.timesteps*self.step, self.step)):
            for obj_id in range(self.total_number_of_objects):
                self.data.append(self._generateValidGroundTruthDataPoint(obj_id=obj_id,
                                                                         ts=ts))

    def writeCsv(self, filename):

        df = pd.DataFrame(self.data)
        df.to_csv(filename, index=False)


class DataManipulator:
    def __init__(self, input_filename, output_filename,
                 sensor_config, threshold, max_noise_points,
                 noise_ranges, add_noise=True):

        self.df_ground_truth = pd.read_csv(input_filename)
        self.columns_original = self.df_ground_truth.columns
        self.sensor_config = sensor_config
        self.threshold = threshold
        self.max_noise_points = max_noise_points
        self.add_noise = add_noise
        self.x_noise_range, self.y_noise_range = noise_ranges
        self.output_filename = output_filename

    @staticmethod
    def add_noise_to_point(row, threshold, sensor):

        point_x = row["x"]
        point_y = row["y"]
        point_shifted = {}

        while True:
            dx = uniform(-threshold/2, threshold/2)
            dy = uniform(-threshold/2, threshold/2)

            radius_squared = (dx*dx) + (dy*dy)
            radius_squared_thrs = (threshold/2)*(threshold/2)

            within_circle = (radius_squared <= radius_squared_thrs)
            sensor_visible = sensor.in_range(x=point_x + dx,
                                             y=point_y + dy)

            if (within_circle and sensor_visible):
                break

        point_shifted["x"] = point_x + dx
        point_shifted["y"] = point_y + dy

        return pd.Series(point_shifted)

    def calcVisiblePoints(self):

        is_visible = self.df_ground_truth.apply(lambda row: self.sensor_config.in_range(x=row['x'],
                                                                                        y=row['y']),
                                                axis=1)
        df_visible = self.df_ground_truth[is_visible].copy()

        return df_visible

    def addNoiseToDataframe(self, dataframe):

        df_tmp = dataframe.apply(lambda row: self.add_noise_to_point(row,
                                                                     threshold=self.threshold,
                                                                     sensor=self.sensor_config),
                                 axis=1)

        df_noised_points = dataframe.copy()
        for col in df_tmp.columns:
            df_noised_points[col] = df_tmp[col]

        return df_noised_points

    def perturbDataPoints(self):

        df_sensor_visible = self.calcVisiblePoints()
        if self.add_noise:
            df_perturbed = self.addNoiseToDataframe(df_sensor_visible)
        else:
            df_perturbed = df_sensor_visible

        return df_perturbed

    def calcRandomPoints(self, dataframe):

        rows = []
        for _, row in dataframe.iterrows():

            noise_points = randint(0, self.max_noise_points)

            for _ in range(noise_points):
                spec = OrderedDict()
                spec["objId"] = int(row["objId"])
                spec["x"] = uniform(*self.x_noise_range)
                spec["y"] = uniform(*self.y_noise_range)
                spec["timestamp"] = int(row['timestamp'])
                rows.append(spec)

        return pd.DataFrame(rows)

    def calculateNoisyDataAndSave(self):

        dataframe_noised = self.perturbDataPoints()
        dataframe_pure_noise = self.calcRandomPoints(dataframe_noised)
        dataframe_joined = pd.concat([dataframe_noised, dataframe_pure_noise])
        dataframe_joined.sort_values(['timestamp', 'objId'], inplace=True)
        dataframe_joined[self.columns_original].to_csv(self.output_filename, index=False)

