import sys

from sensors import SensorConfig
from data import DataManipulator


def main(argv):

    threshold = float(argv[0])

    camera_json = "../config/cameraConfig.json"
    short_radar_json = "../config/shortRangeRadarConfig.json"
    long_radar_json = "../config/longRangeRadarConfig.json"

    input_file = '../dist/expectedOutputData.csv'
    camera_output = '../dist/cameraDataExpectedInput.csv'
    short_radar_output = '../dist/shortRangeRadarDataExpectedInput.csv'
    long_radar_output = '../dist/longRangeRadarDataExpectedInput.csv'

    camera = SensorConfig(camera_json)
    short_radar = SensorConfig(short_radar_json)
    long_radar = SensorConfig(long_radar_json)

    camera_manipulator = DataManipulator(input_filename=input_file,
                                         output_filename=camera_output,
                                         sensor_config=camera,
                                         threshold=threshold,
                                         max_noise_points=2,
                                         noise_ranges=[[-10, 120],
                                                       [-20, 20]],
                                         add_noise=False)

    camera_manipulator.calculateNoisyDataAndSave()

    short_radar_manipulator = DataManipulator(input_filename=input_file,
                                              output_filename=short_radar_output,
                                              sensor_config=short_radar,
                                              threshold=threshold,
                                              max_noise_points=2,
                                              noise_ranges=[[-10, 120],
                                                            [-20, 20]],
                                              add_noise=True)

    short_radar_manipulator.calculateNoisyDataAndSave()

    long_radar_manipulator = DataManipulator(input_filename=input_file,
                                             output_filename=long_radar_output,
                                             sensor_config=long_radar,
                                             threshold=threshold,
                                             max_noise_points=2,
                                             noise_ranges=[[-10, 120],
                                                           [-20, 20]],
                                             add_noise=True)

    long_radar_manipulator.calculateNoisyDataAndSave()


if __name__ == "__main__":
    main(sys.argv[1:])
