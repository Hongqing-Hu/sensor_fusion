import json


class SensorConfig:

    def __init__(self, json_file):
        self.config = self._read_config(json_file)
        self.x_min = None
        self.x_max = None
        self.y_min = None
        self.y_max = None
        self._get_range()

        assert self.x_min < self.x_max
        assert self.y_min < self.y_max

    @staticmethod
    def _read_config(json_file):
        with open(json_file) as json_data:
            return json.load(json_data)

    def _get_range(self):
        self.x_min = self.config['range']['x']['start']
        self.x_max = self.config['range']['x']['end']
        self.y_min = self.config['range']['y']['start']
        self.y_max = self.config['range']['y']['end']

    def in_range(self, item):
        x_in_range = self.x_min < item['x'] < self.x_max
        y_in_range = self.y_min < item['y'] < self.y_max
        return x_in_range and y_in_range

    def in_range(self, x, y):
        x_in_range = self.x_min < x < self.x_max
        y_in_range = self.y_min < y < self.y_max

        return x_in_range and y_in_range

    def are_visible(self, items_list):
        return [item for item in items_list if self.in_range(item)]
