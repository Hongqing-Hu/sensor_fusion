from data import DataCreator
from sensors import SensorConfig


def main():

    camera_json = "../config/cameraConfig.json"
    short_radar_json = "../config/shortRangeRadarConfig.json"
    long_radar_json = "../config/longRangeRadarConfig.json"
    output_file = '../dist/expectedOutputData.csv'

    camera = SensorConfig(camera_json)
    short_radar = SensorConfig(short_radar_json)
    long_radar = SensorConfig(long_radar_json)

    ground_truth_data_creator = DataCreator(camera_config=camera,
                                            short_range_radar_config=short_radar,
                                            long_range_radar_config=long_radar,
                                            total_number_of_objects=3,
                                            step=2,
                                            timesteps=100)

    ground_truth_data_creator.generateGroundTruthData()
    ground_truth_data_creator.writeCsv(filename=output_file)


if __name__ == "__main__":
    main()
